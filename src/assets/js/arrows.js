/*
 * Animation des fleches sur l'acceuil
 * Animation qui se lance et qui s'arrrete pendant 1 seconde.
 */

const arrows = document.querySelectorAll('.home-helper__arrow');

function StopAnimation() {
  arrows.forEach((arrow) => arrow.classList.remove('startAnimation'));
}

function StartAnimation() {
  arrows.forEach((arrow) => arrow.classList.add('startAnimation'));
  setTimeout(StopAnimation, 2600);
}

setInterval(StartAnimation, 2850);
