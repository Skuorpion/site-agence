// Definition des 3 couleurs principales
const colors = [
  '#e38481',
  '#8fC695',
  '#Efcc77',
];

// Définit la couleur principale du site entier aleatoirement parmis 3
const numColor = Math.floor(Math.random() * 3);

//Creation d'une variable css afin de pouvoir reutilise la couleur dominante dans nos fichiers scss
document.querySelector('#jsStyle').innerHTML = `body{--main-color:${colors[numColor]};}`;