/*
 * Carroussel fessant défiller les valeurs de l'équipe.
 * Carroussel vertical avec une description relier a chaque element du carroussel.
 */

// Paramétrage du carroussel
const carousel = EmblaCarousel(
  document.querySelector('.value-slider'),
  {
    loop: true,
    axis: 'y',
    inViewThreshold: 0.5,
  },
);

// Mise en forme de chaque éléments du carroussel appeler à chaque défilement de ce dernier.
function formatting() {
  // On récupère l'element principal du carroussel.
  const indexCurrent = carousel.selectedScrollSnap();

  // Suppresion de l'ancienne mise en forme des elements du carroussel.
  document.querySelector('.firstSlide').classList.remove('firstSlide');
  document.querySelector('.lastSlide').classList.remove('lastSlide');

  const formattedSlides = document.querySelectorAll('.mainSlide, .secondarySlides');

  formattedSlides.forEach((formattedSlide) => {
    formattedSlide.classList.remove('mainSlide');
    formattedSlide.classList.remove('secondarySlides');
  });

  // Ajout de la nouvelle mise en forme.
  document.querySelector(`.value-slider__slide:nth-child(${indexCurrent + 1})`).classList.add('mainSlide');
  document.querySelector(`.value-slider__slide:nth-child(${((indexCurrent + 5) % 7) + 1})`).classList.add('firstSlide');
  document.querySelector(`.value-slider__slide:nth-child(${((indexCurrent + 2) % 7) + 1})`).classList.add('lastSlide');

  const secondarySlides = document.querySelectorAll(`.value-slider__slide:nth-child(${((indexCurrent + 1) % 7) + 1}), .value-slider__slide:nth-child(${((indexCurrent + 6) % 7) + 1})`);
  secondarySlides.forEach((secondarySlide) => {
    secondarySlide.classList.add('secondarySlides');
  });

  // Affiche la bonne légende selon l'élément principal affiché par le carroussel.
  document.querySelector('.caption__item--reveal').classList.remove('caption__item--reveal');
  document.querySelector(`.caption__item:nth-child(${indexCurrent + 1})`).classList.add('caption__item--reveal');
}

// Appel de la fonction de mise en forme lors du scroll du carroussel.
carousel.on('select', formatting);

// Fait défiler le carroussel vers le haut ou le bas suivant la position
// d'un click au sein de ce dernier.
document.querySelector('.value-slider').addEventListener('click', function (event) {
  const positionSlider = this.getBoundingClientRect();
  const y = event.pageY - positionSlider.top;

  if (y > positionSlider.height / 2) {
    carousel.scrollNext();
  } else {
    carousel.scrollPrev();
  }
});
