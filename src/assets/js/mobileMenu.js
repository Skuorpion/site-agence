/*
 * Permet de refermer le menu sur mobile lors d'un click sur un lien.
*/

const links = document.querySelectorAll('.navigation-list__lien');
const checkboxMenu = document.querySelector('#menu');

const CloseMenu = () => {
  checkboxMenu.checked = false;
};

links.forEach((link) => {
  link.addEventListener('click', CloseMenu);
});
