const setupDotBtns = (dotsArray, embla) => {
  dotsArray.forEach((dotNode, i) => {
    dotNode.addEventListener("click", () => embla.scrollTo(i), false);
  });
};

const generateDotBtns = (dots, embla) => {
  const template = document.getElementById("scroll-dot-template").innerHTML;
  dots.innerHTML = embla.scrollSnapList().reduce(acc => acc + template, "");
  return [].slice.call(dots.querySelectorAll(".scroll__dot"));
};

const selectDotBtn = (dotsArray, embla) => () => {
  const previous = embla.previousScrollSnap();
  const selected = embla.selectedScrollSnap();
  dotsArray[previous].classList.remove("is-selected");
  dotsArray[selected].classList.add("is-selected");
};




/*
 * Defini le scroll horizontal sur mobile avec la librairie embla.
 */

let scrollPage;
const scroll = document.querySelector('.scroll');
const scroll__container = document.querySelector('.scroll__container');
const pages = document.querySelectorAll('.page');

function InitScrollPage() {
  if (window.innerWidth > 992) {
    // InitStylePage('mobile', 'desktop');
    scrollPage = EmblaCarousel(
      document.querySelector('.scroll'),
      {
        axis: 'y',
      },
    );
  } else {
    // InitStylePage('desktop', 'mobile');
    scrollPage = EmblaCarousel(
      document.querySelector('.scroll'),
    );
  }

  const dots = document.querySelector(".scroll__dots");
  let dotsArray = generateDotBtns(dots, scrollPage);
  const setSelectedDotBtn = selectDotBtn(dotsArray, scrollPage);
  setupDotBtns(dotsArray, scrollPage);
  scrollPage.on("select", setSelectedDotBtn);
  scrollPage.on("init", setSelectedDotBtn);
}


function InitDotsPage() {
  const dots = document.querySelector(".scroll__dots");
  let dotsArray = generateDotBtns(dots, scrollPage);
  const setSelectedDotBtn = selectDotBtn(dotsArray, scrollPage);
  setupDotBtns(dotsArray, scrollPage);
  scrollPage.on("select", setSelectedDotBtn);
  scrollPage.on("init", setSelectedDotBtn);
}

/* function InitStylePage(remove, add) {
  /*
  scroll.classList.remove(`scroll--${remove}`);
  scroll.classList.add(`scroll--${add}`);
  */
/* scroll__container.classList.remove(`scroll__container--${remove}`);
scroll__container.classList.add(`scroll__container--${add}`);
pages.forEach((page) => {
  page.classList.remove(`page--${remove}`);
  page.classList.add(`page--${add}`);
});
} */

// Lors du scroll sur desktop (avec la molette de la souris ou le pad), scroll de la page
function onWheel(evt) {
  if (evt.deltaY > 0) {
    scrollPage.scrollNext();
  } else if (evt.deltaY < 0) {
    scrollPage.scrollPrev();
  }

  document.removeEventListener('wheel', onWheel);
  window.setTimeout(() => {
    document.addEventListener('wheel', onWheel);
  }, 1750);
}

document.addEventListener('wheel', onWheel);

window.onresize = InitScrollPage;
InitScrollPage();

const removeSlides = (embla, slidesToRemove) => {
  slidesToRemove.forEach((s) => embla.containerNode().removeChild(s));
  embla.reInit();
}

const windowWidth = window.innerWidth;
const slidesToRemove = [];
const navContact = document.querySelector('#navContact');

if(windowWidth < 992) {
  slidesToRemove.push(document.querySelector('#sophrologue'));
  slidesToRemove.push(document.querySelector('#callteaser'));

  navContact.setAttribute('onclick', 'scrollPage.scrollTo(5)');
} else {
  slidesToRemove.push(document.querySelector('#projects'));
  navContact.setAttribute('onclick', 'scrollPage.scrollTo(6)');
}

removeSlides(scrollPage, slidesToRemove);
InitDotsPage();