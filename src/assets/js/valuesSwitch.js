const checkbox = document.querySelector('.values-switch__checkbox');
const nav_select = document.querySelector('.values-switch');
const values_list = document.querySelector('.values-list');

function switchClass() {
    nav_select.classList.toggle('values-switch--value');
    values_list.classList.toggle('values-list--desc')
}

checkbox.addEventListener('change', switchClass)