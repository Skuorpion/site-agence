const burger = document.querySelector('.header__burger');
const inputs = document.querySelectorAll('.project__input');
const logo_mobile = document.querySelector('.header__logo--mobile');
const logo_desktop = document.querySelector('.header__logo--desktop');

function HideMenu(input) {
  if (input.target.checked) {
    // Si on ouvre l'information d'un projet :
    //    - cacher le menu burger
    //    - désactiver le scroll horizontal
    burger.style.zIndex = '0';
    scrollPage.reInit({ draggable: false });
  } else {
    // Lors de la fermeture de l'information d'un projet :
    //    - réafficher le menu burger après l'animation
    //    - réactiver le scroll horizontal
    setTimeout(function () { burger.style.zIndex = '20' }, 400);
    scrollPage.reInit({ draggable: true });
  }
}

// Réactivation du scroll horizontal ainsi que le réaffichage du menu burger
function backToHomePage() {
  scrollPage.reInit({ draggable: true });
  scrollPage.scrollTo(0);
  burger.style.zIndex = '20';
  inputs.forEach((input) => {
    input.checked = false;
  });
}

// Cacher le menu burger lors de l'ouverture d'un descriptif de projet
inputs.forEach((input) => {
  input.addEventListener('change', HideMenu, input);
});

logo_mobile.addEventListener('click', backToHomePage);
logo_desktop.addEventListener('click', backToHomePage);
